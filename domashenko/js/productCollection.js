/**
 * Created by megabatz on 10.09.2016.
 */
function ProductCollection() {
    var productName = document.getElementById('productName');
    var productPrice = document.getElementById('productPrice');
    var createButton = document.getElementById('createButton');
    var placeHTML = document.getElementById('productSell');

    this.create = function () {
        //1
        var name = productName.value;
        var price = productPrice.value;
        var newProduct =  {
            name: name,
            price: price
        };
        //2
        var data = this.getData();
        data.push(newProduct);
        this.saveData(data);
        this.render();
    };
    this.saveData = function (data) {
        localStorage.setItem('product', JSON.stringify(data));
    };
    this.render = function () {
        var data = this.getData();
        var productsArr = data.map(function (product) {
            return new ProductToSell(product).createCollection();
        });
        var products = productsArr.reduce(function (baseFragment, currentFragment) {
            baseFragment.appendChild(currentFragment);
            return baseFragment;
        }, document.createDocumentFragment());
        placeHTML.appendChild(products);
    };
    this.getData = function () {
        var localData = localStorage.getItem('product');
        if (localData) {
            return JSON.parse(localData);
        } else {
            return [];
        }
    };
    this.init = function () {
        //1
        this.render();
        //2 
        createButton.addEventListener('click', this.create.bind(this));

    };
}