var products = [
  {
    "name":"coffee",
    "price": 12,
    "image": "coffee.jpg",
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
    "maxPrice": 18,
    "minPrice": 8
  },
  {
    "name": "wheat",
    "price": 10,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
    "maxPrice": 20,
    "minPrice": 8
  },  
  {
    "name": "steel",
    "price": 18,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
    "maxPrice": 20,
    "minPrice": 8
  }
]

module.exports = products;