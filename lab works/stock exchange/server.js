var express = require("express");
var bodyParser = require("body-parser");
var products = require("./products.js");
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.get('/', function (req, res) {
  res.sendfile('index.html');
});
app.get('/products', function (req, res) {
    res.send(JSON.stringify(products));
});
app.get('/products/:name', function (req, res) {
  var model;
  products.forEach(function (product, index, list) {
    if (product.name === req.params.name) {
      model = product;
    }
  });
  res.send(JSON.stringify(model));
});
app.delete('/products/:name', function (req, res) {
  products.forEach(function (product, index, list) {
    if (product.name === req.params.name) {
      list.splice(index, 1);
    }
  });
  res.sendStatus(200);
});
app.put('/products/:name', function (req, res) {
  if (req.body.price < 0) {
    res.sendStatus(400);
  } else {
    products.forEach(function (product, index, list) {
      if (product.name === req.params.name) {
          list[index] = req.body;
      }
    });
    setTimeout(function () {
      res.sendStatus(200);
    }, 4000);
  }
});
app.post('/products', function (req, res) {
    products.push(req.body);
    res.status(200);
    res.send({
        status: 200,
        body: req.body
    });
});

app.use('/css', express.static('css'));
app.use('/js', express.static('js'));
app.use('/fonts', express.static('fonts'));

app.listen(8080, function () {
  console.log('Listening on 8080');
});