function ProductDetailsController (options) {
  var $element = $('#mainElement');
  var model;

  this.clean = $.noop;
  this.render = function () {
      //creating html with model data
      //using template (view)
      var html = productDetails({
        productName: model.get('name'),
        productPrice: model.get('price'),
        imgSrc: model.get('image'),
        productDescription: model.get('description'),
        maxPrice: model.get('maxPrice'),
        minPrice: model.get('minPrice')
      });

      $element.html(html);

      return $element;
    }
  if (options.name) {
    model = new ProductModel({name: options.name});
    model.on('fetch', this.render, this);
    model.fetch();
  } else {
    model = options.model;
  }
 
  $element.on('click','.js-close', function () {
    mediator.publish('state:clickCloseDetails');
  })
}