//base model with observer implementation
var observerMixin = {
  on: function (event, callback, context) {
    this.subscribers[event].push({
      callback: callback,
      context: context
    });
  },
  trigger: function (event, data) {
    this.subscribers[event].forEach(function (info) {
      info.callback.call(info.context, data);
    });
  },
  off: function (event, object) {
    var offSubscribers;
    if (object) {
      offSubscribers = _.where(this.subscribers[event], {context: object});
      this.subscribers[event] = _.difference(this.subscribers[event], offSubscribers);
    } else {
      this.subscribers[event] = [];
    }
  }
}