//this is an entry point of our application

//we init products controller here which takes care of our products section
//var productsController = new ProductCollectionController().init();
var mediator = new Mediator();
(function () {
  var stateManager = new StateManager().init();
})();
//if we will have more sections here - we will add another controller initing here