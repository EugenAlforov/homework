function ProductModel (options) {
  var data = {};
  if (options) {
    data = {
      name: options.name,
      price: options.price,
      image: options.image,
      description: options.description,
      minPrice: options.minPrice,
      maxPrice: options.maxPrice
    }
  }
  //default events with empty subscribers
  this.subscribers = {
    'delete': [],
    'deleted failed': [],
    'invalid': [],
    'valid': [],
    'updated': [],
    'updated failed': [],
    'syncing': [],
    'sync': [],
    'saved': [],
    'saved failed': [],
    'fetch failed': [],
    'fetch': []
  }
  this.get = function (key) {
    return data[key];
  }
  this.getData = function () {
    return {
      name: data.name,
      price: data.price,
      image: data.image
    }
  }
  this.getAdditionalData = function () {
    return {
      description: data.description,
      minPrice: data.minPrice,
      maxPrice: data.maxPrice
    }
  }
  this.getFullData = function () {
    var fullData = _.extend(this.getData(),this.getAdditionalData());
    
    return fullData;
  }
  this.set = function (key, value) {
    data[key] = value;

    return this;
  }
  this.fetch = function (callback) {
    $.ajax({
      method: 'GET',
      url: '/products/' + data.name,
      context: this,
      success: function (newData) {
        data = JSON.parse(newData);
        this.trigger('fetch');
      },
      error: function () {
        this.trigger('fetch failed', {name: data.name});
      },
      complete: function () {
        this.trigger('sync');
      }
    });
  }

  this.update = function (newData) {
    //updating data on client side firstly-
    if (this.validate(newData)) {
      //triggering 'valid' for controller to remove an error
      this.trigger('valid');
      data.name = newData.name;
      data.price = newData.price;
      //sending request with updated data to save changes on server
      this.trigger('syncing');
      //using jquery instead of ajaxSender - more options
      $.ajax({
        method: 'PUT',
        url: '/products/' + data.name,
        body: JSON.stringify(this.getData()),
        context: this,
        //if updated was ok - trigger updated
        success: function () {
          this.trigger('updated');
        },
        //if updated was not ok - trigger failed
        error: function () {
          this.trigger('updated failed');
        },
        //when request is back - trigger sync 
        complete: function () {
          this.trigger('sync');
        }
      });
    } else {
      //trigger invalid to show user error
      this.trigger('invalid');
    }
  }
  this.save = function (newData) {
    if (this.validate(newData)) {
      //triggering 'valid' for controller to remove an error
      this.trigger('valid');
      data.name = newData.name;
      data.price = newData.price;
      //sending request with updated data to save changes on server
      this.trigger('syncing');
      //using jquery instead of ajaxSender - more options
      $.ajax({
        method: 'POST',
        url: '/products',
        data: JSON.stringify(this.getData()),
        contentType: 'application/json; charset=UTF-8',
        context: this,
        //if updated was ok - trigger updated
        success: function () {
          this.trigger('saved');
        },
        //if updated was not ok - trigger failed
        error: function () {
          this.trigger('saved failed');
        },
        //when request is back - trigger sync 
        complete: function () {
          this.trigger('sync');
        }
      });
    } else {
      //trigger invalid to show user error
      this.trigger('invalid');
    }
  }
  this.validate = function (data) {
    var priceRegExp = /^\$?[-\d]{1,4}\$?$/;
    var nameRegExp = /^\w{3,20}$/;
    var valid = true;

    valid = priceRegExp.test(data.price) && nameRegExp.test(data.name);

    return valid;
  }
    
  this.delete = function (callback) {
    $.ajax({
      method: 'DELETE',
      url: '/products/' + data.name,
      context: this,
      success: function () {
        this.trigger('delete', {name: data.name});
      },
      error: function () {
        this.observer.trigger('delete failed', {name: data.name});
      },
      complete: function () {
        this.trigger('sync');
      }
    });
  }
}
//inheriting from base model
mixin(ProductModel.prototype, observerMixin, 'observer');