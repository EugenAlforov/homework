var ProductController = (function () {
  var amountOfProducts = 0;//static variable for all products

  //receiving a model from collection controller
  return function (model) {

    //creating an element, which will be used to render a model
    var $el = $('<div>');
    //increasing static variable
    //counter of products amount
    //we will use it in future, currently not used
    amountOfProducts++;
    //subscribing on different events of model
    //to make our html has the same state as model
    model.on('delete', function () {
      $el.remove();
    });
    //subscribing on invalid event of model
    //when model is nvalid - we show error
    model.on('invalid', function () {
      $el.find('.js-error').addClass('is-shown');
    });
    model.on('valid', function () {
      $el.find('.js-error').removeClass('is-shown');
    });
    model.on('syncing', function () {
      $el.addClass('is-loading');
    });
    model.on('sync', function () {
      $el.removeClass('is-loading');
    });
    model.on('updated', function () {
      this.render();
    }, this);
    model.on('updated failed', function () {
      $el.find('.js-serverError').removeClass('is-shown');
    });
    model.on('deleted failed', function () {
      $el.find('.js-serverError').removeClass('is-shown');
    });
    this.render = function () {
      //creating html with model data
      //using template (view)
      $el.addClass('content-item');

      var html = productView({
        productName: model.get('name'),
        productPrice: model.get('price'),
        imgSrc: model.get('image')
      });

      $el.html(html);

      return $el;
    }
    function showDetails () {
      //empty method to show details
      //we will fullfill it on the next lesson
      mediator.publish('state:clickProductDetails', model);
    }
    function deleteProduct (event) {
      event.stopPropagation();

      model.delete();
    }

    function updateProduct () {
      //finding inputs by id
      //
      var $priceInput = $el.find('#productPrice');
      var $nameInput = $el.find('#productName');
      //updating model,
      //getting data to update from inputs
      //passing render method as a callback
      model.update({
        name: $nameInput.val(),
        price: $priceInput.val()
      });
    }

    this.renderEditView = function () {
      var content = productEditView({
        productName: model.get('name'),
        productPrice: model.get('price')
      });

      $el.html(content);


      return $el;
    }
    //adding event listeners to buttons
    //buttons may be rerendered, that's why we may lose click handler,
    //listening for clicks only on parent element (event delegation)
    $el.on('click', function () {
      showDetails();
    });
    $el.on('click', '.js-submit', function () {
      updateProduct.call(this);
    }.bind(this));
    $el.on('click', '.js-delete', function (event) {
      event.stopPropagation();
      deleteProduct.call(this, event);
    }.bind(this));
    $el.on('click', '.js-edit', function () {
      this.renderEditView.call(this);
    }.bind(this));
  }
})();