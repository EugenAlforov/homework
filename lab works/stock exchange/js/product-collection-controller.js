function ProductCollectionController () {
  //creating collectoin which will take care of data 
  var collection = new ProductCollection();
  var $createButton = $('#createProduct');
  var $priceInput = $('#productPrice');
  var $nameInput = $('#productName');

  //finding an element on the page which we will work with
  var $element = $('#mainElement');
  
  //init method which does all required actions when initing
  this.init = function () {
    //subscribing on collection 'fetched' event

    collection.on('syncing', this.toggleBusy.bind(this, true), this);
    collection.on('sync', this.toggleBusy.bind(this, false));
    collection.on('created', this.renderOne, this);
    collection.on('created failed', this.toggleServerError.bind(this, true));
    collection.on('fetched', this.render.bind(this));
    collection.on('valid', this.toggleError.bind(this, false));
    collection.on('invalid', this.toggleError.bind(this, true));
    //asking collection to fetch
    collection.fetchData();

    return this;
  }
  this.create = function () {
      var newName = $nameInput.val();
      var newPrice = $priceInput.val();
      collection.create({
        name: newName,
        price: newPrice
      });
      this.toggleServerError(false);
  };
  this.clean = function () {
    collection.off('syncing');
    collection.off('sync');
    collection.off('created');
    collection.off('created failed');
    collection.off('fetched');
    collection.off('valid');
    collection.off('invalid');
  }
  this.toggleBusy = function (isBusy) {
    $element.toggleClass('is-loading', isBusy);
  };
  this.render = function () {
    var formHtml = productCollectionView();
    //creating an html fragment with block of products
    var productsHtml = collection.models.map(function (productModel) {
      //for each model we create a controller
      //and delegate to him duty to render model
      //and handle users input 
      var controller = new ProductController(productModel);

      return controller.render();
    });

    $element.html(productsHtml);
    $element.prepend(formHtml);
  }
  this.renderOne = function (model) {
    var controller = new ProductController(model);
    $element.append(controller.render());

  }
  this.toggleError = function (isError) {
    $('.js-newProductForm .js-error').toggleClass('is-shown', isError);
  }

  this.toggleServerError = function(isError) {
    $('.js-newProductForm .js-serverError').toggleClass('is-shown', isError);
  }

  var clickHandler = this.create.bind(this);
  this.destroy = function () {
    $createButton.off('click', clickHandler);
  }
   $createButton.on('click', clickHandler);
}