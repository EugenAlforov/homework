function MyPromise(executor) {
  var callbacks = [];
  var status = 'pending';
  var savedData;

  executor(resolve);
  this.then = function (callback) {
    if (status === 'pending') {
      callbacks.push(callback);
    } else if (status === 'fullfilled') {
      callback(savedData);
    }
  }
  function resolve (data) {
    status = 'fullfilled';
    savedData = data;
    callbacks.forEach(function(cb){
      cb(data);
    });
  }
}
var somePromise = new MyPromise(function (resolve, reject) {
  setTimeout(function () {
    resolve('some data');
  }, 2000);
  
}); 

somePromise.then(function (data) {
  console.log('first listener: ' + data);
}, function (reason) {
  console.log(reason);
});
somePromise.then(function (data) {
  console.log('another listener: ' + data);
});

setTimeout(function () {
  somePromise.then(function (data) {
    console.log('third listener: ' + data);
  });
}, 3000);