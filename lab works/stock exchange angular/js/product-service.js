
angular.module('stockExchange').service('ProductService', ProductService);
ProductService.$inject = ['$q', '$http'];
function ProductService ($q, $http) {
  var data;
  var fetched;
  this.getData = function () {
    var promise;
    
    if (fetched) {
      promise = $q(function (resolve, reject) {
        resolve(data);
      });
    } else {
      promise = $http.get('/products').then(function (response) {
        data = response.data;
        fetched = true;
        return data;
      });
    }
    return promise;
  }
  this.getProductByName = function (name) {
    return this.getData().then(function (products) {
      var productByName;
      products.forEach(function (product) {
        if (product.name === name) {
          productByName = product;
        }
      });
      return productByName;
    });
  }
  this.save = function (data) {
    return $http.post('/products', data).then(function (response) {

      return response.data;
    });
  }
}