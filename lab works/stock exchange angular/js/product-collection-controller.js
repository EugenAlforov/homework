angular.module('stockExchange').controller('ProductCollectionController', ProductCollectionController);
ProductCollectionController.$inject = ['$scope', 'ProductService', '$state'];
function ProductCollectionController($scope, products, $state) {
  $scope.invalid = false;
  $scope.serverError = false;
  products.getData().then(function (data) {
    $scope.productCollection = data;
  });

  $scope.newProduct = {
    price: '',
    name: ''
  }
  $scope.add = function () {
    var copy = angular.copy($scope.newProduct);
    
    products.save(copy).then(function (data) {
      
    });
  }
  $scope.edit = function (product) {
    console.log(product);
    product.isEditing = true;
  }
  $scope.submit = function (product) {
    product.isEditing = false;
  }
  $scope.remove = function (product) {
    products.remove(product);
  };
  $scope.showDetails = function (name) {
    $state.go('details', {name: name});
  };
}
