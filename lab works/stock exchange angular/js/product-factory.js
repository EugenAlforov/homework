
angular.module('stockExchange').factory('ProductFactory', ProductFactory);
ProductFactory.$inject = ['$q', '$http'];
function ProductFactory ($q, $http) {
  var data;
  var productsCollection = {};

  console.log('factory is created');

  productsCollection.getData = function () {
    return $http.get('/products').then(function (response) {
      data = response.data;

      return data;
    });
  }
  productsCollection.save = function (data) {
    return $http.post('/products', data).then(function (response) {
      console.log(response);
      return response.data;
    });
  }
  productsCollection.remove = function (product) {
    var index = data.indexOf(product);
    data.splice(index, 1);
  }

  return productsCollection;
}