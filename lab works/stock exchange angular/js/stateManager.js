function StateManager() {
  this.renderCollection = function () {
    this.currentController && this.currentController.clean();
    this.currentController = new ProductCollectionController().init();
    window.location.hash = 'products';
  }
  this.renderDetails = function (info) {
    this.currentController && this.currentController.clean();
    if (typeof info === 'string') {
      this.currentController = new ProductDetailsController({name: info});
      window.location.hash = 'products/' + info;
    } else {
      this.currentController = new ProductDetailsController({model: info});
      window.location.hash = 'products/' + info.get('name');
    } 
    
    this.currentController.render();
  }
  this.init = function () {
    var state = window.location.hash || '#products';
    var self = this;
    
    var states = [
      {
        callback: function () {
          self.renderCollection();
        },
        pattern: /^#products$/i
      },
      {
        callback: function () {
          self.renderDetails('coffee'); 
        },
        pattern: /^#products\/(\w+)$/i
      }
    ];

    var notFound = true;
    for (var i = 0; i < states.length && notFound; i++) {
      if (states[i].pattern.test(state)) {
        states[i].callback();
        notFound = false;
      }
    }
    return this;
  }
  mediator.subscribe('state:clickProductDetails', this.renderDetails, this);
  mediator.subscribe('state:clickCloseDetails', this.renderCollection, this);
}





