angular.module('stockExchange', ['ui.router']);

angular.module('stockExchange').config(function($stateProvider) {
  $stateProvider.state('home', {
    url: '/',
    templateUrl: '/views/product-collection.html',
    controller: 'ProductCollectionController',
  }).state('details', {
    url: '/details/:name',
    templateUrl: '/views/details.html',
    controller: 'ProductDetailsController',
    resolve: {
      product: function (ProductService, $stateParams) {
        return ProductService.getProductByName($stateParams.name);
      }
    }
  });
});