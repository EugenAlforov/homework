function ProductCollection () {
  //default events and empty arrays of subscribers
  this.subscribers = {
    'fetching': [],
    'fetched': [],
    'sync': [],
    'syncing': [],
    'created': [],
    'created failed': [],
    'valid': [],
    'invalid': []
  };
  this.create = function (data) {
    var newModel = new ProductModel();
    newModel.on('syncing', function () {
     this.trigger('syncing');
    },this);
    newModel.on('valid', function () {
      this.trigger('valid');
    }, this);
    newModel.on('invalid', function () {
      this.trigger('invalid');
    }, this);
    newModel.on('saved', function () {
      this.trigger('created', newModel);
      this.models.push(newModel);
    }, this);
    newModel.on('sync', this.trigger.bind(this, 'sync'));
    newModel.on('saved failed', function () {
      this.trigger('created failed');
    }, this);

    newModel.save(data);
  };
  this.deleteModel = function (eventData) {
    //looking for model index
    var modelIndex = _.findIndex(this.models, function (model) {
      return model.get('name') === eventData.name;
    });
    this.models.splice(modelIndex, 1);
    console.log(this.models);
  }

  this.fetchData = function () {
    var data;
    //sending request to server
    this.trigger('fetching');
    
    return new Promise(function (resolve, reject) {
      $.ajax({
        method: 'GET',
        url: '/products  ',
        context: this,
        success: function (response) {
          data = JSON.parse(response);
          this.handleData(data);
          resolve(data);
          //triggering 'fetched' for controller to render
        },
        error: function () {
          reject('server didn\'t respond');
        }
      });
    });
    subscribers.forEach(function(cb) {
      cb();
    });
  }
  this.handleData = function (newData) {
    this.models = newData.map(function (product) { 
      var model = new ProductModel(product);

      model.on('delete', this.deleteModel, this);

      return model;
    }, this);
  }
}
//mixing in from observer
// 'on' and 'trigger' methods
mixin(ProductCollection.prototype, observerMixin);

