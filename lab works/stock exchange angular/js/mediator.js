function Mediator () {
  var subscribers = {};
  this.subscribe = function (event, callback, context) {
    if (subscribers[event] instanceof Array) {
      subscribers[event].push({
        callback: callback,
        context: context
      });
    } else {
      subscribers[event] = [];
      subscribers[event].push({
        callback: callback,
        context: context
      });
    }
    
  }
  this.publish = function (event, data) {
    if (subscribers[event] instanceof Array) {
      subscribers[event].forEach(function (info) {
        info.callback.call(info.context, data);
      });
    }
  }
}