(function () {
  angular.module('stockExchange').directive('seProduct', function () {
    return {
      restrict: 'E',
      scope: {
        myProduct: '=productAttr',
        edit: '&editAttr'
      },
      templateUrl: 'views/product.html',
      controller: function ($scope, $timeout) {
        console.log($scope.edit);
      }
    }
  });
})();