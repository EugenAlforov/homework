module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-webfont');

  grunt.initConfig({
    less: {
      development: {
        files: {
          'css/style.css': 'css/style.less'
        } 
      }
    },
    watch: {
      files: "css/*.less",
      tasks: ['less']
    },
    webfont: {
      icons: {
          src: 'icons/*.svg',
          dest: 'fonts',
          destCss: 'css',
          options: {
            autoHint: false,
            relativeFontPath: '/fonts/',
            engine: 'node'
          }
      }
    },
    uglify: {
      my_target: {
        files: {
          'bundle.min.js': ['js/*.js']
        }
      }
    }
  });

  grunt.registerTask('default', ['less', 'watch']);
}