var sandwichNumber = 0;
var juiceNumber = 0;
var sandwichPrice = 2; 
var juicePrice = 1;
var orderPrice = 0;

document.getElementById('start-order').onclick = function () {
	sandwichNumber = 0;
	juiceNumber = 0;
	orderPrice = 0;
	alert('New order!');
}

document.getElementById('button-sandwich').onclick = function () {
	sandwichNumber++;
	alert('number of sandwich is ' + sandwichNumber);
}

document.getElementById('button-juice').onclick = function () {
	juiceNumber++;
	alert('number of juice is ' + juiceNumber);
}

document.getElementById('finish-order').onclick = function () {
orderPrice = sandwichNumber * sandwichPrice + juiceNumber * juicePrice;

	if (orderPrice < 3) {
		alert(orderPrice + '$. There are not enough items for an order!');
	}
	else if (orderPrice > 7) {
		alert(orderPrice + '$. There are too many items for one order!');
	}
	else {
		alert('Total price of order is ' + orderPrice + '$');
	}
}
