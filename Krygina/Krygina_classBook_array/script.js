var studentLabels = document.getElementsByClassName('student');
var classBook = [];
var i;

// fill array of button`s data
for (i = 0; i < studentLabels.length; i++) {
	classBook.push({
		attended: false,
		name: studentLabels[i].innerHTML,
		toggleAttended: function () {
			this.attended = !this.attended;
		}
	});
}
// loop listens of button click, and change status of button attended
for (i = 0; i < studentLabels.length; i++) {
	studentLabels[i].addEventListener('click', function () {
		var nameToFind = this.innerHTML;

		classBook.forEach(function (student) {
			if (student.name === nameToFind) {
				student.toggleAttended();
				console.log(student);
			}
		});
		updateClassBook();
	});
}
// function updates list of student names
function updateClassBook () {
	var attendedNames = [];     
	console.log(attendedNames);

	classBook.forEach(function (student) {
		console.log(student.attended);
		if (student.attended === true) { 
			console.log('in');
			attendedNames.push(student.name);
			console.log(attendedNames);
		}
	});
	document.getElementById('selected').innerHTML = attendedNames.join(', ');
}
