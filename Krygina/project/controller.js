var section = document.getElementById('collection');

cards.forEach( function (card) {
	var items = [];
	var content = [card.word, card.translate, 'Edit', 'Delete'];
	var br = document.createElement('br');
	var amountItems = cards.length + 1;

	for (var i = 0; i < amountItems; i++) {
		items[i] = document.createElement('div');
		items[i].innerHTML = content[i];

		if (i < 2) {
			items[i].className = 'cardView';
		} else {
			items[i].className = 'buttonEditDelete';	
		}
		section.appendChild(items[i]);
	};
		section.appendChild(br);
});