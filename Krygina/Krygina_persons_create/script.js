var runButton = document.getElementById('run');
var argumentsInput = document.getElementById('argumentsInput');
var displayElement = document.getElementById('display');

function Person (name, age) {	
	this.personsName = name;
	this.personsAge = age; 

	this.aboutme = function () {
		return '. My name is ' + this.personsName + 
		'. I am ' + this.personsAge;
	}
}

var teacherPerson = new Person('Andrew', 44);
var studentPerson = new Person('Patric', 11);
var parentPerson = new Person('Jessica', 42);

var roles = {
	teacher: {
		personRole: 'TEACHER',
		personsName: teacherPerson,
		introduce: function () {
			return 'Hi, I am a '+ this.personRole + this.personsName.aboutme();
		}
	},
	student: {
		personRole: 'STUDENT',
		personsName: studentPerson,
		introduce: function () {
			return 'Hi, I am a ' + this.personRole + this.personsName.aboutme();
		}
	},
	parent: {
		personRole: 'PARENT',
		personsName: parentPerson,
		introduce: function () {
			return 'Hi, I am a ' + this.personRole + this.personsName.aboutme();
		}
	},
	clickHandler: function () {
		var role = document.getElementById('role').value;
		var methodName = document.getElementById('method').value;
		var params = document.getElementById('argumentsInput').value.split(',');
		
		var person = this[role];
		var method = methods[methodName];
		method.apply(person, params);	
	}
}

var methods = {
	sayWords: function () {
		var output = this.introduce() + '<br/>';
    	output += 'Words are: ';

    	[].forEach.call(arguments, function (arg) {
    	output +=  ' ' + arg;
    	});
		displayElement.innerHTML = output;
	},
	sayFirstWord: function (word) {
		var output = this.introduce() + '<br/>';
    	output+= 'First word is: ' + word;
		displayElement.innerHTML = output;
	}
}

runButton.addEventListener('click', roles.clickHandler.bind(roles));

