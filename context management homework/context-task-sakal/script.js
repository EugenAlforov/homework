var runButton = document.getElementById('run');
var argumentsInput = document.getElementById('argumentsInput');
var displayElement = document.getElementById('display');


function Person(name, age, role) {
  this.personName = name;
  this.age = age;
  this.role = role;
  this.introduce = function () {
    return 'Hi, I am a ' + this.role.toUpperCase() + ', my name is ' + this.personName;
  }
}

var roles = {
  teacher: {
    person: new Person('Andrew', 44, 'teacher'),
  },
  student: {
    person: new Person('Patric', 11, 'student'),
  },
  parent: {
    person: new Person('Jessica', 42, 'parent'),
  },
  clickHandler: function () {
    var role = document.getElementById('role').value;
    var methodName = document.getElementById('method').value;
    var params = document.getElementById('argumentsInput').value.split(','); //array of params
    var person = this[role];
    var method = methods[methodName];

    method.apply(person, params);
  }
}

var methods = {
  sayWords: function () {
    var output = this.person.introduce() + '<br/>';

    output += 'Words are: ';

    [].forEach.call(arguments, function (arg) {
      output += ' ' + arg;
    });

    displayElement.innerHTML = output;

  },
  sayFirstWord: function (word) {
    var output = this.person.introduce() + '<br/>';

    output += 'First word is: ' + word;
    displayElement.innerHTML = output;
  }
}
runButton.addEventListener('click', roles.clickHandler.bind(roles));




