var runButton = document.getElementById('run');
var argumentsInput = document.getElementById('argumentsInput');
var displayElement = document.getElementById('display');

var roles = {
  teacher: {
    personsName: 'Andrew',
    age: 44,
    introduce: function () {
      return 'Hi, I am a TEACHER, my name is ' + this.personsName;
    }
  },
  student: {
    personsName: 'Patric',
    age: 11,
    introduce: function () {
      return 'Hi, I am a STUDENT, my name is ' + this.personsName;
    }
  },
  parent: {
    personsName: 'Jessica',
    age: 42,
    introduce: function () {
      return 'Hi, I am a PARENT, my name is ' + this.personsName;
    }
  },
  clickHandler: function () {
    var role = document.getElementById('role').value;
    var methodName = document.getElementById('method').value;
    var params = document.getElementById('argumentsInput').value.split(','); //array of params

    var person = this[role];
    var method = methods[methodName];

    method.apply(person, params);
  }
}

var methods = {
  sayWords: function () {
    var output = this.introduce() + '<br/>';

    output += 'Words are: ';

    [].forEach.call(arguments, function (arg) {
      output +=  ' ' + arg;
    });

    displayElement.innerHTML = output;
 
  },
  sayFirstWord: function (word) {
    var output = this.introduce() + '<br/>';

    output+= 'First word is: ' + word;
    displayElement.innerHTML = output;
  }
}
runButton.addEventListener('click', roles.clickHandler.bind(roles));

