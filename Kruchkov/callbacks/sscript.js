var studentsLabels = document.getElementsByClassName('student');
var classBook = [];
var attendedStudents = [];
var i;

for (i=0;i < studentsLabels.length; i++) {
	classBook.push({
			attended: false,
			name: studentsLabels[i].innerHTML,
			toogleAttended: function() {
				this.attended = !this.attended;
			}
		});
}

for (i = 0; i < studentsLabels.length; i++) {
	studentsLabels[i].addEventListener('click', function() {
		
		var nameToFind = this.innerHTML;
		
		classBook.forEach(function(student) {
			if (student.name===nameToFind) {
				console.log('student name ' + student.name + '  is correct');
				student.toogleAttended();
			} 
		});
		updateClassBook();		
	});
}

function updateClassBook() {
	var attendedNames = [];

	classBook.forEach(function(student) {
		if (student.attended) {
			attendedNames.push(student.name);
		}

	});
	 if (attendedNames.length===classBook.length)
	document.getElementById('classBook').innerHTML = attendedNames.join();
}