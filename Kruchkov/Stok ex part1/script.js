var productsSection = document.getElementById('productsSection');
var products = [
	{
		name: 'coffee',
		price: 12,
		image: 'coffee.jpg'
	},
	{
		name: 'wheat',
		price: 7,
		image: 'wheat.jpg'
	}
];

products.forEach(function(product) {
	var divProduct = document.createElement('div');
	var bTagInProduct = document.createElement('b');
	var imgTagInProduct = document.createElement('img');

	divProduct.innerHTML = product.name;
	divProduct.className = 'productContainer';
	divProduct.dataset.price = product.price;
	divProduct.dataset.name = product.name;

	bTagInProduct.innerHTML = ' - ' + product.price + '$';

	imgTagInProduct.src = product.image;
	imgTagInProduct.alt = product.name;

	divProduct.appendChild(bTagInProduct);
	divProduct.appendChild(imgTagInProduct);


	productsSection.appendChild(divProduct);

	divProduct.addEventListener('click', function(e) {
			console.log('Price of ' + e.currentTarget.dataset.name + ' is ' + e.currentTarget.dataset.price + '$');
		}
	);
});