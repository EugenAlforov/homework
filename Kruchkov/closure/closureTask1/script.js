var sum;
var multiply;

function createCalcFunction () {

	sum = function (a, b) {
		var result; 

		result = a + b; 

		return 'Result is: ' + result;
	}
	multiply = function (a, b) {
		var result; 

		result = a * b;
		
		return 'Result is: ' + result;
	}
}
createCalcFunction();

document.getElementById('sum').addEventListener('click', function () {
	var operand1 = Number(document.getElementById('operand1').value);
	var operand2 = Number(document.getElementById('operand2').value);
	alert(sum(operand1, operand2));
});

document.getElementById('multiply').addEventListener('click', function () {
	var operand1 = Number(document.getElementById('operand1').value);
	var operand2 = Number(document.getElementById('operand2').value);
	alert(multiply(operand1, operand2));
});
