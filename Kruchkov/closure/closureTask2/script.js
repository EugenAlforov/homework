var sum;
var multiply;


function createCalcFunction () {
	var timesUsed = 0;
	sum = function (a, b) {
		var result; 
		var output; 

		timesUsed++;
		result = a + b;
		
		output = 'Answer: ' + result + '. Used ' + timesUsed + ' times';

		return output;
	}
	multiply = function (a, b) {
		var result; 
		var output;

		timesUsed++;
		result = a * b;
		
		output = 'Answer: ' + result + '. Used ' + timesUsed + ' times';

		return output;
	}
}
createCalcFunction();

document.getElementById('sum').addEventListener('click', function () {
	var operand1 = Number(document.getElementById('operand1').value);
	var operand2 = Number(document.getElementById('operand2').value);
	alert(sum(operand1, operand2));
});

document.getElementById('multiply').addEventListener('click', function () {
	var operand1 = Number(document.getElementById('operand1').value);
	var operand2 = Number(document.getElementById('operand2').value);
	alert(multiply(operand1, operand2));
});
