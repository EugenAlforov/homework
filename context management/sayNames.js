var childNameButton = document.getElementById('sayNameChild');
var teacherNameButton = document.getElementById('sayNameTeacher');

var child = {
	name: 'Alex',
	surname: 'First',
	age: 14,
	sayName: function () {
		console.log(this.name, this.surname);
	}
}

var teacher = {
	teacherName: 'John',
	age: 38,
	sayName: function () {
		console.log(this.teacherName);
	}
}


childNameButton.addEventListener('click', child.sayName.bind(child));

teacherNameButton
.addEventListener('click', teacher.sayName.bind(teacher));
