var runButton = document.getElementById('run');
var argumentsInput = document.getElementById('argumentsInput');
var displayElement = document.getElementById('display');
var roles;

function Person (name, age) {
  this.personsName = name;
  this.age = age;
  this.introduce = function () {
    return 'Hi, I am a ' + role.value + ', my name is ' + this.personsName;
  };
}

roles = {
  teacher: new Person('Andrew', 26),
  parent: new Person('Bill', 40),
  student: new Person('David', 19),
  clickHandler: function () {
    var role = document.getElementById('role').value;
    var methodName = document.getElementById('method').value;
    var params = document.getElementById('argumentsInput').value.split(',');
    var person = this[role];
    var method = methods[methodName];
    method.apply(person, params);
  }
};

var methods = {
  sayWords: function () {
    var output = this.introduce() + '<br/>';

    output += 'Words are: ';
    [].forEach.call(arguments, function (arg) {
      console.log(arg);
      output +=  ' ' + arg;
    });

    displayElement.innerHTML = output;

  },
  sayFirstWord: function (word) {
    var output = this.introduce() + '<br/>';

    output+= 'First word is: ' + word;
    displayElement.innerHTML = output;
  }
};
runButton.addEventListener('click', roles.clickHandler.bind(roles));
