var studentLabels = document.getElementsByClassName('student');
var classBook = [];
var i;

function updateClassBook () {
  var attendedNames = [];
  var classBookFilled = document.getElementById('classBook').innerHTML;

  classBook.forEach(function (student) {
    if (student.attended) {
      attendedNames.push(student.name);
    }
  });

  if (attendedNames.length == 0) {
    classBookFilled = 'No students attended!';
  }
  else if (attendedNames.length == studentLabels.length) {
    classBookFilled = 'Everyone attended!'
  }
  else {
    classBookFilled = attendedNames.join(', ');
  }

  document.getElementById('classBook').innerHTML = classBookFilled;
}


for (i = 0; i < studentLabels.length; i++) {
  classBook.push({
    name: studentLabels[i].value,
    attended: false,
    toggleAttended: function () {
      this.attended = !this.attended;
    }
  });
}

for (var i = 0; i < studentLabels.length; i++) {
  studentLabels[i].addEventListener('click', function() {
    var nameToFind = this.value;
    classBook.forEach(function(student) {
      if (student.name === nameToFind) {
        student.toggleAttended();
      }
    });
    updateClassBook();
  });
}

updateClassBook();
