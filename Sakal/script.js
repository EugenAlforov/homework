/**
 * Created by tiagat on 13.08.16.
 */

var status_bar = document.getElementById('attended');
var students = document.getElementsByClassName('student');

var attended = {
    students: {},
    toString : function () {
        var list = [];
        for (var key in this.students)
        {
            if (this.students[key].attended) list.push(this.students[key].name);
        }
        return list.join(', ');
    }
}

function Person(name) {
    this.name = name;
    this.attended = false;

    this.clickEvent = function () {
        this.attended = !this.attended;
        console.log(' ' + attended);
    }
}

for (var i = 0; i < students.length; i++) {
    var person = new Person(students[i].innerHTML);

    attended.students[person.name] = person;
    students[i].person = person;
    students[i].addEventListener('click', function () {
        this.person.clickEvent();
        status_bar.innerHTML = attended.toString();
    })
}
