var sandwitchCount = 0;
var juiceCount = 0;

var sandwitchCoust = 2;
var juiceCoust = 1;


function displayCounter() {
	document.getElementById("item-sandwitch-counter").innerText = sandwitchCount;
	document.getElementById("item-juice-counter").innerText = juiceCount;
}

function resetCounter(){
	sandwitchCount = 0;
	juiceCount = 0;
	displayCounter();
}

function getTotalPrice() {
	return (sandwitchCoust * sandwitchCount +  juiceCoust * juiceCount);
}

document.getElementById('item-sandwitch').onclick = function() {
	sandwitchCount++;
	displayCounter();
	
}

document.getElementById('item-juice').onclick = function() {
	juiceCount++;
	displayCounter();
}


document.getElementById('order-start').onclick = function() {
	resetCounter();
}

document.getElementById('order-finish').onclick = function() {
	var total = getTotalPrice();

	if (total < 3) {
		alert('Not enough items for an order');
	}
	else if (total > 7) {
		alert('Too many items for one order');
	}
	else 
	{
		alert('Total cost: ' + total + '$\nGood appetite!');
		resetCounter();
	}
}

